This is the developer's log document, also called read me for White-box testing with TDD

Hongbin Sun
Hattan Baraqan
*******************------------------------********************

The purpose of the project is to practice test-drive development(TDD) with equivalence 
practicing (EP) as well as boundary value analysis(BVA). 

Overall, the functionality including a mock database object, with Item getItem(String ItemID), the 
database object will return an Item object, from where another method float getPrice() is 
called and the corresponding price of that item will be returned. 

There is another class named shoppingCart, when the method 
calcPurchasePrice(ArrayList<Item> ProductIDs, Customer customer) is called, it will return
a string including the original price, discount, tax and final price as shown in the method. 

We started the project by creating a test class for Customer, since it is the easiest one to 
test our TDD and implementation are correct. After creating boolean isShoppingClub(Customer customer)
method, we set some random customer object and return true or false. After that, we added
setter and getter to encapsulate the class. Similarly, we used the same strategy to test setting
a price for a specific item and return the price, before we filled in the Item class and 
getPrice() method.

After confirming this TDD and implementation are successful in our environment, we created the test
case for Database object and Item object. In short, we set the ItemID for a Database object, and
also set the ItemID and price for an Item object, and returned the price from the Database object, 
and tested this against the expected price before we filled in the Database object and methods.

Next, after we have successfully tested we were able to get a price from a database object, we 
moved next to create shoppingCart class and filled in the calcPurchasePrice() method using TDD.
Here, we created an ArrayList<Item> to represent the shopping cart, and filled in with different
Item objects with some mock price, like 1.00. In short, we tried to cover different aspects of the
tests cases according to the functionality of the story, including test all the conditions for
a customer, and all different conditions for shopping cart size. We have also covered some boundary
value analysis dealing with the if conditions. In addition, we have also added empty shopping cart
condition. Of particular interest to notice, if an item is not set in the database, the test
case will return a null pointer exception, suggesting this item is not present in the database.It
is not listed in the test cases, even if we encountered many times. 

We have commented the purpose of each test case as we created them along the way, and modified 
the method if the test failed. Something worth mentioning is that, the first test case was a 
little hard to create, so we created a User class with main method and printed out the real result 
and figured out where the problem is. There is no meaningful value for User class in our homework,
it simply served as a debugging tool. 

Last but not the least, due to the time constrain and other issues, we did not create interface
in our project because the dependencies are simple and the classes are self-explaining. However, 
this could be something for further development for this project. 