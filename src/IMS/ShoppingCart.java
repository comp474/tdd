package IMS;

import java.util.ArrayList;
import java.util.List;




public class ShoppingCart{
	
	
	public String calcPurchasePrice(ArrayList<Item> ProductIDs, Customer customer){
		
		double total=0.00;//this is the original price without discount
		double discount = 0.00;//this is the discount amount due to membership etc. 
		//double taxRate = 0.045;//tax = 4.5%
		double tax = 0.00;//tax amount
		double finalprice=0.00;//final price
	
		for(Item item: ProductIDs) {
			total+= item.getPrice();
		}
		//System.out.println(total);
		//System.out.println(ProductIDs.size());

		if (ProductIDs.size()>=10){
		
			discount += total*0.1;}
			
		else if (ProductIDs.size()>5 && ProductIDs.size()<10){
			discount +=total*0.05;}

		else if (ProductIDs.size()>50){
			//System.out.println("Please load less than 50 items");
			return null;		
		}
		
		if(ProductIDs.size()>50 || ProductIDs.isEmpty()){
			return null;
		}
		else{
	
		
		if (customer.isShoppingClub(customer.customerID)==true){
			discount += total*0.1;}
		else {
			discount +=0;}
			
		if(customer.isTaxExempt(customer.getCustomerID())==false){
			tax = (total-discount) *0.045;}
		else {
			tax =0.00; }
		
		finalprice = total-discount+tax;
		
		
		String total_form = String.format("%.2f", total);//original price without discount
		String discount_form = String.format("%.2f", discount);//discount due to membership etc
		String tax_form = String.format("%.2f", tax);//tax amount
		String finalprice_form = String.format("%.2f", finalprice);//the final price after discount and tax
			
		return total_form +" "+ discount_form +" "+ tax_form + " "+ finalprice_form;
		//return total_form;
		//System.out.print("your original price without discount is "+ total_form);
		//System.out.print("your discount is "+ discount_form);
		//System.out.print("your tax amount is "+ tax_form);
		//System.out.print("your net total due is "+ finalprice_form);
		}
		
	}
	
	
}
