package IMS;

public class Customer {
	
	 public String customerID;
	 public boolean shoppingClub;
	 public boolean taxExempt;
	
	 
	 // Setter and Getter for Customers and their account at the store
	 
	public void setCustomerID(String customerID){
		this.customerID = customerID;
	}
	

	public String getCustomerID(){
		return customerID;
	}
	
	public void setShoppingClub(boolean shoppingClub){
		this.shoppingClub = shoppingClub;
	}
	
	public void setTaxExempt(boolean taxExempt){
		this.taxExempt = taxExempt;
	}
	
	public boolean isShoppingClub(String customerID){
		
		return shoppingClub;
	}
	
	public boolean isTaxExempt(String customerID){
		return taxExempt;
	}

}
