package Testing;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import IMS.*;
import junit.framework.Assert;

public class ShoppingCartTest {

	private DatabaseDAO db;

	@SuppressWarnings({ "deprecation", "unused" })
	/* test1-4, the shopping card is normal(within 50), but the customer is either a shopping club member
	or is eligible for tax exempt. There might be duplicate test with the following group, but for now
	please just ignore and treat them as separate groups
	
	test5-15, the customer does not matter, test different conditions about the cart size, 
	the following size should be tested: 1,  5, 6, 7, 9, 10, 11, 49, 50, 51. 
	
	test15-18, boundary value analysis, to when will the program break, test the following cart size
	155, 0, 10000000, -10000000
	
	test19 some complex conditional tests that left from above  */
	

	@Test //test1: customer1 is not shoppingClub member, but is tax exempt. 
	public void taxExamptTest() {
		DatabaseDAO db1 = new DatabaseDAO();
		DatabaseDAO db2 = new DatabaseDAO();
		DatabaseDAO db3 = new DatabaseDAO();
		DatabaseDAO db4 = new DatabaseDAO();
		DatabaseDAO db5 = new DatabaseDAO();
		
		Customer customer1 = new Customer();
		customer1.setShoppingClub(false);
		customer1.setTaxExempt(true);
		ShoppingCart sc1 = new ShoppingCart();
	
		Item item1 = new Item("101001", (float)1.00);
		Item item2 = new Item("101002", (float)2.00);
		Item item3 = new Item("101003", (float)3.00);
		Item item4 = new Item("101004", (float)4.00);
		Item item5 = new Item("101005", (float)5.00);
		
		db1.setItem(item1);
		db2.setItem(item2);
		db3.setItem(item3);
		db4.setItem(item4);
		db5.setItem(item5);
		
		
		ArrayList<Item> shoppingcart1 = new ArrayList<Item>();
		
			for(int i=0 ; i<10; i++){
				shoppingcart1.add(db1.getItem("101001"));} //10
		
		shoppingcart1.add(db2.getItem("101002")); //12
		shoppingcart1.add(db3.getItem("101003"));//15
		shoppingcart1.add(db3.getItem("101003"));//18
		shoppingcart1.add(db4.getItem("101004"));//22
		shoppingcart1.add(db5.getItem("101005"));//27
		//shoppingcart1.size()==15, get 10% discount, discount==2.7
		//no membership discount
		//has tax exempt 
		
		String result1= sc1.calcPurchasePrice(shoppingcart1, customer1);
		
		assertEquals("27.00 2.70 0.00 24.30", result1);
		
	}
	
	@Test //test2, customer2 is a shopping club member, but not eligible for tax exempt 
	public void shoppingClubMembershipTest() {
		DatabaseDAO db1 = new DatabaseDAO();
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer2 = new Customer();
		customer2.setShoppingClub(true);
		customer2.setTaxExempt(false);
		
		ShoppingCart sc2 = new ShoppingCart();
		
		Item item1 = new Item("101001", (float)1.00);
		Item item2 = new Item("101001", (float)2.00);
		
		db1.setItem(item1);
		db2.setItem(item2);
	
		ArrayList<Item> shoppingcart2 = new ArrayList<Item>();
		
			for(int i=0 ; i<10; i++){
				shoppingcart2.add(db1.getItem("101001"));} //price==10
			shoppingcart2.add(db2.getItem("101002")); //price==12
		
		//shoppingcart2.size()==11, get 10% discount, discount==1.20
		//shoppingclub membership, get 10% discount, discount==1.20
		//no tax exempt, tax==0.432;
		//total ==12-2.4+0.432=10.03
		
		String result2= sc2.calcPurchasePrice(shoppingcart2, customer2);
		
		assertEquals("12.00 2.40 0.43 10.03", result2);
		
	}
	
	@Test //test3, customer3 is neither a shopping club member nor eligible for tax exempt 
	public void shoppingClubMembershipTaxExemptTest() {
		DatabaseDAO db1 = new DatabaseDAO();
		DatabaseDAO db2 = new DatabaseDAO();
		
		Customer customer3 = new Customer();
		customer3.setShoppingClub(false);
		customer3.setTaxExempt(false);
		
		ShoppingCart sc3 = new ShoppingCart();
		
		Item item1 = new Item("101001", (float)1.00);
		Item item2 = new Item("101002", (float)2.00);
		
		db1.setItem(item1);
		db2.setItem(item2);
		
		ArrayList<Item> shoppingcart3 = new ArrayList<Item>();
		
			for(int i=0 ; i<10; i++){
				shoppingcart3.add(db1.getItem("101001"));} //price==10
			shoppingcart3.add(db2.getItem("101002")); //price==12
		
		//shoppingcart2.size()==11, get 10% discount, discount==1.20
		//no tax exempt, tax==0.486;
		//total ==12-1.2+0.486=11.29
		
		String result3= sc3.calcPurchasePrice(shoppingcart3, customer3);
		
		assertEquals("12.00 1.20 0.49 11.29", result3);
		
	}
	
	@Test //test4, customer4 is both a shoppingclub member and also eligible for tax exempt
	public void shoppingClubMembershipTaxExemptTest2() {
		DatabaseDAO db1 = new DatabaseDAO();
		DatabaseDAO db2 = new DatabaseDAO();
		
		Customer customer4 = new Customer();
		customer4.setShoppingClub(false);
		customer4.setTaxExempt(true);
		
		ShoppingCart sc4 = new ShoppingCart();
		
		Item item1 = new Item("101001", (float)1.00);
		Item item2 = new Item("101002", (float)2.00);
		
		db1.setItem(item1);
		db2.setItem(item2);
	
		
		ArrayList<Item> shoppingcart4 = new ArrayList<Item>();
		
			for(int i=0 ; i<10; i++){
				shoppingcart4.add(db1.getItem("101001"));} //price==10
			shoppingcart4.add(db2.getItem("101002")); //price==12
		
		//shoppingcart2.size()==11, get 10% discount, discount==1.20
		//no shoppingclub membership discount
		//tax exempt
		
		//total ==12-1.2=10.80
		
		String result4= sc4.calcPurchasePrice(shoppingcart4, customer4);
		
		assertEquals("12.00 1.20 0.00 10.80", result4);
		
	}
	
	@Test //test5, customer5 has 1 item in the shoppingcart, should just be the only price
	public void oneItemTest() {
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer5 = new Customer();
		customer5.setShoppingClub(false);
		customer5.setTaxExempt(false);
		ShoppingCart sc5 = new ShoppingCart();
		
		Item item2 = new Item("101002", (float)2.00);
		
		db2.setItem(item2);
	
		
		ArrayList<Item> shoppingcart5 = new ArrayList<Item>();
		
		
			shoppingcart5.add(db2.getItem("101002")); //price==2
		
		
		//no shoppingclub membershi discount
		//no tax exempt, tax==0.09
		
		//total ==2
		
		String result5= sc5.calcPurchasePrice(shoppingcart5, customer5);
		
		assertEquals("2.00 0.00 0.09 2.09", result5);
		
	}
	
	@Test //test6, customer6 has 5 items in the shoppingcart, no discount
	public void fiveItemTest() {
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer6 = new Customer();
		customer6.setShoppingClub(false);
		customer6.setTaxExempt(false);
		ShoppingCart sc6 = new ShoppingCart();
		
		Item item2 = new Item("101002", (float)2.00);
		
		db2.setItem(item2);
	
		
		ArrayList<Item> shoppingcart6 = new ArrayList<Item>();
		
		for(int i=0; i<5; i++){
			shoppingcart6.add(db2.getItem("101002"));} //price==2*5==10
		
		
		//no shoppingclub membershi discount
		//no tax exempt, tax==0.45
		
		//total ==10
		
		String result6= sc6.calcPurchasePrice(shoppingcart6, customer6);
		
		assertEquals("10.00 0.00 0.45 10.45", result6);
		
	}
	
	@Test //test7, customer7 has 6 items in the shoppingcart, get 5% discount
	public void sixItemTest() {
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer7 = new Customer();
		customer7.setShoppingClub(false);
		customer7.setTaxExempt(false);
		ShoppingCart sc7 = new ShoppingCart();
		
		Item item2 = new Item("101002", (float)2.00);
		
		db2.setItem(item2);
	
		ArrayList<Item> shoppingcart7 = new ArrayList<Item>();
		
		for(int i=0; i<6; i++){
			shoppingcart7.add(db2.getItem("101002"));} //price==2*6==12
		//5% discount, discount == 0.6
		
		//no shoppingclub membershi discount
		//no tax exempt, tax==(12-0.6)*0.045==0.513
		
		//total ==12-0.6==11.4
		
		String result7= sc7.calcPurchasePrice(shoppingcart7, customer7);
		
		assertEquals("12.00 0.60 0.51 11.91", result7);
		
	}
	
	@Test //test8, customer8 has 7 items in the shoppingcart, get 5% discount
	public void sevenItemTest() {
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer8 = new Customer();
		customer8.setShoppingClub(false);
		customer8.setTaxExempt(false);
		ShoppingCart sc8 = new ShoppingCart();
		
		Item item2 = new Item("101002", (float)2.00);
		
		db2.setItem(item2);
	
		
		ArrayList<Item> shoppingcart8 = new ArrayList<Item>();
		
		for(int i=0; i<7; i++){
			shoppingcart8.add(db2.getItem("101002"));} //price==2*7==14
		//5% discount, discount == 0.7
		
		//no shoppingclub membershi discount
		//no tax exempt, tax==0.5985
		
		//total ==14-0.7+0.5985==13.90
		
		String result8= sc8.calcPurchasePrice(shoppingcart8, customer8);
		
		assertEquals("14.00 0.70 0.60 13.90", result8);
		
	}
	
	@Test //test9, customer9 has 9 items in the shoppingcart, get 5% discount
	public void nighItemTest() {
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer9 = new Customer();
		customer9.setShoppingClub(false);
		customer9.setTaxExempt(false);
		ShoppingCart sc9 = new ShoppingCart();
		
		Item item2 = new Item("101002", (float)2.00);
		
		db2.setItem(item2);
	
		
		ArrayList<Item> shoppingcart9 = new ArrayList<Item>();
		
		for(int i=0; i<9; i++){
			shoppingcart9.add(db2.getItem("101002"));} //price==2*9==18
		//5% discount, discount == 0.9
		
		//no shoppingclub membershi discount
		//no tax exempt, tax==0.7695
		
		//total ==18-0.9+0.7695==17.8695
		
		String result9= sc9.calcPurchasePrice(shoppingcart9, customer9);
		
		assertEquals("18.00 0.90 0.77 17.87", result9);
		
	}
	
	
	@Test //test10, customer10 has 10 items in the shoppingcart, get 10% discount
	public void tenItemTest() {
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer10 = new Customer();
		customer10.setShoppingClub(false);
		customer10.setTaxExempt(false);
		ShoppingCart sc10 = new ShoppingCart();
		
		Item item2 = new Item("101002", (float)2.00);
		
		db2.setItem(item2);
	
		
		ArrayList<Item> shoppingcart10 = new ArrayList<Item>();
		
		for(int i=0; i<10; i++){
			shoppingcart10.add(db2.getItem("101002"));} //price==2*10==20
		//10% discount, discount == 2
		
		//no shoppingclub membershi discount
		//no tax exempt, tax==0.81
		
		//total ==20-2+0.81==18.81
		
		String result10= sc10.calcPurchasePrice(shoppingcart10, customer10);
		
		assertEquals("20.00 2.00 0.81 18.81", result10);
		
	}
	
	@Test //test11, customer11 has 11 items in the shoppingcart, get 10% discount
	public void elevenItemTest() {
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer11 = new Customer();
		customer11.setShoppingClub(false);
		customer11.setTaxExempt(false);
		ShoppingCart sc11 = new ShoppingCart();
		
		Item item2 = new Item("101002", (float)2.00);
		
		db2.setItem(item2);
	
		
		ArrayList<Item> shoppingcart11 = new ArrayList<Item>();
		
		for(int i=0; i<11; i++){
			shoppingcart11.add(db2.getItem("101002"));} //price==2*11==22
		//10% discount, discount == 2.2
		
		//no shoppingclub membershi discount
		//no tax exempt, tax==0.891
		
		//total ==22-2.2+0.891==20.691
		
		String result11= sc11.calcPurchasePrice(shoppingcart11, customer11);
		
		assertEquals("22.00 2.20 0.89 20.69", result11);
		
	}
	
	@Test //test12, customer13 has 49 items in the shoppingcart, get 10% discount, no error
	public void fourty_nighItemTest() {
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer12 = new Customer();
		customer12.setShoppingClub(false);
		customer12.setTaxExempt(false);
		ShoppingCart sc12 = new ShoppingCart();
		
		Item item2 = new Item("101002", (float)2.00);
		
		db2.setItem(item2);
	
		
		ArrayList<Item> shoppingcart12 = new ArrayList<Item>();
		
		for(int i=0; i<49; i++){
			shoppingcart12.add(db2.getItem("101002"));} //price==2*49==98
		//10% discount, discount == 9.8
		
		//no shoppingclub membershi discount
		//no tax exempt, tax==3.969
		
		//total ==98-9.8+3.969==92.169
		
		String result12= sc12.calcPurchasePrice(shoppingcart12, customer12);
		
		assertEquals("98.00 9.80 3.97 92.17", result12);
		
	}
	
	@Test //test13, customer13 has 50 items in the shoppingcart, get 10% discount, no error
	public void fiftyItemTest() {
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer13 = new Customer();
		customer13.setShoppingClub(false);
		customer13.setTaxExempt(false);
		ShoppingCart sc13 = new ShoppingCart();
		
		Item item2 = new Item("101002", (float)2.00);
		
		db2.setItem(item2);
	
		
		ArrayList<Item> shoppingcart13 = new ArrayList<Item>();
		
		for(int i=0; i<50; i++){
			shoppingcart13.add(db2.getItem("101002"));} //price==2*50==100
		//10% discount, discount == 10
		
		//no shoppingclub membershi discount
		//no tax exempt, tax==4.05
		
		//total ==100-10+4.05==94.05
		
		String result13= sc13.calcPurchasePrice(shoppingcart13, customer13);
		
		assertEquals("100.00 10.00 4.05 94.05", result13);
		
	}
	
	@Test //test14, customer14 has 51 items in the shoppingcart, return null
	public void fifty_oneItemTest() {
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer14 = new Customer();
		customer14.setShoppingClub(false);
		customer14.setTaxExempt(false);
		ShoppingCart sc14 = new ShoppingCart();
		
		Item item2 = new Item("101002", (float)2.00);
		
		db2.setItem(item2);
	
		
		ArrayList<Item> shoppingcart14 = new ArrayList<Item>();
		
		for(int i=0; i<51; i++){
			shoppingcart14.add(db2.getItem("101002"));} //more than 50 items, return null
			
		String result14= sc14.calcPurchasePrice(shoppingcart14, customer14);
		
		assertEquals(null, result14);
		
	}


	@Test //test15, the customer15 has 155 items in the shoppingcart, return null
	public void overLoadTest(){
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer15 = new Customer();
		customer15.setShoppingClub(false);
		customer15.setTaxExempt(false);
		ShoppingCart sc15 = new ShoppingCart();
		
		Item item6 = new Item("101006", (float)6.00);
		db2.setItem(item6);
		
		ArrayList<Item> shoppingcart15 = new ArrayList<Item>();
		
		for (int i=0; i<155; i++){
			shoppingcart15.add(db2.getItem("101006"));
		}
		//expcet the method return null since the shoppintcart2.size()>50;
		String result16 = sc15.calcPurchasePrice(shoppingcart15, customer15);
		
		assertEquals(null, result16);
		
	}
	
	@Test //test16, the customer is empty, return null
	public void emptyTest(){
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer16 = new Customer();
		customer16.setShoppingClub(false);
		customer16.setTaxExempt(false);
		ShoppingCart sc16 = new ShoppingCart();
		
		Item item6 = new Item("101006", (float)6.00);
		db2.setItem(item6);
		
		ArrayList<Item> shoppingcart16 = new ArrayList<Item>();
		
		//expcet the method return null since the shoppingcart is empty
		String result16 = sc16.calcPurchasePrice(shoppingcart16, customer16);
		
		assertEquals(null, result16);
		
	}
	
	@Test //test17, the customer17 has 100000000 items in the shoppingcart, return null
	public void extremeLoadTest(){
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer17 = new Customer();
		customer17.setShoppingClub(false);
		customer17.setTaxExempt(false);
		ShoppingCart sc17 = new ShoppingCart();
		
		Item item6 = new Item("101006", (float)6.00);
		db2.setItem(item6);
		
		ArrayList<Item> shoppingcart17 = new ArrayList<Item>();
		
		for (int i=0; i<10000000; i++){
			shoppingcart17.add(db2.getItem("101006"));
		}
		//expcet the method return null since the shoppintcart2.size()>50;
		String result17 = sc17.calcPurchasePrice(shoppingcart17, customer17);
		
		assertEquals(null, result17);
		
	}
	

	@Test //test18, the customer18 has -10000000 items in the shoppingcart, return null
	public void underLoadTest(){
		DatabaseDAO db2 = new DatabaseDAO();
		Customer customer18 = new Customer();
		customer18.setShoppingClub(false);
		customer18.setTaxExempt(false);
		ShoppingCart sc18 = new ShoppingCart();
		
		Item item6 = new Item("101006", (float)6.00);
		db2.setItem(item6);
		
		ArrayList<Item> shoppingcart18 = new ArrayList<Item>();
		
		for (int i=0; i<-10000000; i++){
			shoppingcart18.add(db2.getItem("101006"));
		}
		//expcet the method return null since the shoppintcart2.size()>50;
		String result18 = sc18.calcPurchasePrice(shoppingcart18, customer18);
		
		assertEquals(null, result18);
		
	}
	

	@Test //test19: customer19 is shoppingClub member, not tax exmept. 
	public void complexTest() {
		DatabaseDAO db1 = new DatabaseDAO();
		DatabaseDAO db2 = new DatabaseDAO();
		DatabaseDAO db3 = new DatabaseDAO();
		DatabaseDAO db4 = new DatabaseDAO();
		DatabaseDAO db5 = new DatabaseDAO();
		Customer customer19 = new Customer();
		customer19.setShoppingClub(true);
		customer19.setTaxExempt(false);
		ShoppingCart sc19 = new ShoppingCart();
	
		Item item1 = new Item("101001", (float)1.00);
		Item item2 = new Item("101002", (float)2.00);
		Item item3 = new Item("101003", (float)3.00);
		Item item4 = new Item("101004", (float)4.00);
		Item item5 = new Item("101005", (float)5.00);
		
		db1.setItem(item1);
		db2.setItem(item2);
		db3.setItem(item3);
		db4.setItem(item4);
		db5.setItem(item5);
		
		
		ArrayList<Item> shoppingcart19 = new ArrayList<Item>();
		
			for(int i=0 ; i<15; i++){
				shoppingcart19.add(db1.getItem("101001"));} //15
		
		shoppingcart19.add(db2.getItem("101002")); //17
		shoppingcart19.add(db3.getItem("101003"));//20
		shoppingcart19.add(db3.getItem("101003"));//23
		shoppingcart19.add(db4.getItem("101004"));//27
		shoppingcart19.add(db5.getItem("101005"));//32
		//shoppingcart1.size()==15, get 10% discount, discount==3.2
		//membership discount, discount ==3.2
		//no tax exempt, tax == (32-3.2-3.2)*0.045==1.152
		
		String result19= sc19.calcPurchasePrice(shoppingcart19, customer19);
		
		assertEquals("32.00 6.40 1.15 26.75", result19);
	}

}
