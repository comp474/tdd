package Testing;

import static org.junit.Assert.*;

import org.junit.Test;

import IMS.Customer;
import junit.framework.Assert;

public class CustomerTest {

	@SuppressWarnings("deprecation")
	@Test
	public void testCustomerShoppingClub() {
		//fail("Not yet implemented");
		
		
		// Creating Customers with ID and setters
		
		Customer customer1 = new Customer();
		customer1.shoppingClub=true; 
		customer1.taxExempt= true;
		customer1.customerID="201001";
		
		Customer customer2 = new Customer();
		customer2.shoppingClub = false;
		customer2.taxExempt = false;
		customer2.customerID = "201002";
		
		Customer customer3 = new Customer();
		customer3.setCustomerID("201003");
		customer3.setShoppingClub(true);
		customer3.setTaxExempt(false);
		
		
		//Testing shopping club membership
		assertTrue(customer1.isShoppingClub(customer1.customerID));
		assertFalse(customer2.isShoppingClub(customer2.customerID));
		assertTrue(customer3.isShoppingClub(customer3.getCustomerID()));
		
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testCustomerTaxExempt(){
		//fail("Not yet impelemnted");
		
		Customer customer1 = new Customer();
		customer1.shoppingClub=true; 
		customer1.taxExempt= true;
		customer1.customerID="201001";
		
		Customer customer2 = new Customer();
		customer2.shoppingClub = false;
		customer2.taxExempt = false;
		customer2.customerID = "201002";
		
		Customer customer3 = new Customer();
		customer3.setCustomerID("201003");
		customer3.setShoppingClub(true);
		customer3.setTaxExempt(false);
		
		//Testing TaxExempt
		assertTrue(customer1.isTaxExempt(customer1.customerID));
		assertFalse(customer2.isTaxExempt(customer2.customerID));
		assertFalse(customer3.isTaxExempt(customer3.getCustomerID()));
	
		
	}

	

}
