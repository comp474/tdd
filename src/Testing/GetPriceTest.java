package Testing;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import IMS.Customer;
import IMS.Item;
import IMS.ShoppingCart;
import IMS.DatabaseDAO;
import junit.framework.Assert;

public class GetPriceTest {

	@SuppressWarnings("deprecation")
	@Test
	public void testGetPrice1() {//just test Item class and return price of that item object
		//fail("Not yet implemented");
		
		Item item1 = new Item("101001", (float)21.54);
		Item item2 = new Item("101002", (float)12.32);
		Item item3 = new Item("101003", (float)0.09);
		
		//set ID and price directly into the constructor 
		assertEquals(item1.getPrice(), (float)21.54, 0);
		assertEquals(item2.getPrice(), (float)12.32, 0);
		assertEquals(item3.getPrice(), (float)0.09, 0);
		
		
	}
	
	@Test
	public void testGetItem(){
		//fail("Not yet implemented");
		
		
		// Create a database "DatabaseDAO" object ( has String ItemID, Item item)
		DatabaseDAO db1 = new DatabaseDAO();
	
		// Create Item object and set it to the database and casting price to float
		Item item4 = new Item("101004", (float)4.00);
		db1.setItem(item4);
		
		// test the get item
		assertEquals(db1.getItem("101004"), item4);
		
	}
	
	@Test
	public void testGetPrice2(){
		//fail("not yet implemented");
		
		DatabaseDAO db2 = new DatabaseDAO();
		
		Item item4 = new Item("101005", (float)5.00);
		
		db2.setItem(item4);
		
		// Test the get price and should get 5.00
		assertEquals(db2.getItem("101005").getPrice(), (float)5.00, 0);
		
	}
	
	@Test
	public void addShoppingCartTest(){
		//fail("Not yet implemented");
		
		
		//Create a database object
		DatabaseDAO db3 = new DatabaseDAO();
		
		// create an item object
		Item item4 = new Item("101004", (float)4.00);
		// add the item object to the database object 
		db3.setItem(item4);
		
		
		/*Create a shoppingcart arrayList with item objects gotten from the database object 
		to test the arraylist is working, and separate calcPurchasePrice() */
		
		ArrayList<Item> shoppingcartresult = new ArrayList<Item>();
		for(int j=0; j<5; j++){
		shoppingcartresult.add(db3.getItem("101004"));}
		
		//expected result to verify the database returns item with arraylist
		ArrayList<Item> sc1 = new ArrayList<Item>();
		for(int i=0; i<5; i++){
			sc1.add(item4);
		}
		
		assertEquals(shoppingcartresult, sc1);
	}
	
	
}

